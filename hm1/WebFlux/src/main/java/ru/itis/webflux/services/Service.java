package ru.itis.webflux.services;

import reactor.core.publisher.Flux;
import ru.itis.webflux.entities.Book;

public interface Service {
    Flux<Book> getBooks();
}
