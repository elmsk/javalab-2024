package ru.itis.webflux.clients;

import reactor.core.publisher.Flux;
import ru.itis.webflux.entities.Book;

public interface Client {
    Flux<Book> getBooks();
}
