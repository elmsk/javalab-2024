package ru.itis.webflux.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import ru.itis.webflux.clients.Client;
import ru.itis.webflux.entities.Book;

import java.util.List;

@Component
@AllArgsConstructor
public class BookServiceImpl implements Service {

    private final List<Client> clients;

    @Override
    public Flux<Book> getBooks() {
        List<Flux<Book>> fluxes = clients.stream().map(this::getBooks).toList();
        return Flux.merge((fluxes));
    }

    private Flux<Book> getBooks(Client client) {
        return client.getBooks()
                .subscribeOn(Schedulers.boundedElastic());
    }

}
