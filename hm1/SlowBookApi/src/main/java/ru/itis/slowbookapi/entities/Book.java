package ru.itis.slowbookapi.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class Book {
    private String title;
    private String author;
    private String description;
}
