package ru.itis.slowbookapi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.slowbookapi.entities.Book;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {

    @GetMapping()
    public List<Book> getAllBooks() throws InterruptedException {
        Thread.sleep(5000);
        return List.of(
                Book.builder()
                        .title("Ася")
                        .author("Тургенев И.С.")
                        .description("Повесть Тургенева. Написана в 1857 году")
                        .build(),
                Book.builder()
                        .title("Демиан")
                        .author("Герман Гессе")
                        .description("Роман немецкого писателя Германа Гессе, впервые опубликованный в 1919 году")
                        .build(),
                Book.builder()
                        .title("Мёртвые души")
                        .author("Гоголь Н.В.")
                        .description("Поэма, которая должна была выйти в трёх томах")
                        .build()
        );
    }
}
