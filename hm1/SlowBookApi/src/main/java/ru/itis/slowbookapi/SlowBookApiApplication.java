package ru.itis.slowbookapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SlowBookApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SlowBookApiApplication.class, args);
    }

}
