package ru.itis.bookapi.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;

@AllArgsConstructor
@Builder
public class Book {
    private String title;
    private String author;
    private String description;
}
