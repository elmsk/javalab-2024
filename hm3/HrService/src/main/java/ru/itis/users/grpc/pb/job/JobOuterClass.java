// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: job.proto

package ru.itis.users.grpc.pb.job;

public final class JobOuterClass {
  private JobOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_JobRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_JobRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_Job_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_Job_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_JobResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_JobResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\tjob.proto\"\014\n\nJobRequest\"n\n\003Job\022\r\n\005titl" +
      "e\030\001 \001(\t\022\017\n\007company\030\002 \001(\t\022\016\n\006salary\030\003 \001(\r" +
      "\022\022\n\nexperience\030\004 \001(\r\022\023\n\013description\030\005 \001(" +
      "\t\022\016\n\006skills\030\006 \003(\t\"%\n\013JobResponse\022\026\n\010list" +
      "Jobs\030\001 \003(\0132\004.Job23\n\nJobService\022%\n\006GetJob" +
      "\022\013.JobRequest\032\014.JobResponse\"\000B\035\n\031ru.itis" +
      ".users.grpc.pb.jobP\001b\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_JobRequest_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_JobRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_JobRequest_descriptor,
        new java.lang.String[] { });
    internal_static_Job_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_Job_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_Job_descriptor,
        new java.lang.String[] { "Title", "Company", "Salary", "Experience", "Description", "Skills", });
    internal_static_JobResponse_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_JobResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_JobResponse_descriptor,
        new java.lang.String[] { "ListJobs", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
