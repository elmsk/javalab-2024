package ru.itis.users.service.impl;

import net.devh.boot.grpc.client.inject.GrpcClient;
import ru.itis.users.dto.JobDto;
import ru.itis.users.grpc.pb.job.Job;
import ru.itis.users.grpc.pb.job.JobRequest;
import ru.itis.users.grpc.pb.job.JobResponse;
import ru.itis.users.grpc.pb.job.JobServiceGrpc;
import ru.itis.users.service.JobService;

import java.util.List;

public class JobServiceGrpcClientImpl implements JobService {

    @GrpcClient("job-service")
    private JobServiceGrpc.JobServiceBlockingStub client;
    @Override
    public List<JobDto> getJobs() {
        JobResponse resp = client.getJob(JobRequest.getDefaultInstance());

        return resp.getListJobsList().stream().map(this::map).toList();
    }

    protected JobDto map(Job job) {
        return JobDto.builder()
                .title(job.getTitle())
                .company(job.getCompany())
                .salary(job.getSalary())
                .description(job.getDescription())
                .experience(job.getExperience())
                .skills(job.getSkillsList())
                .build();
    }
}
