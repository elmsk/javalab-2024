package ru.itis.users.grpc.pb.cv;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.9.1)",
    comments = "Source: cv.proto")
public final class CvServiceGrpc {

  private CvServiceGrpc() {}

  public static final String SERVICE_NAME = "CvService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getGetCvMethod()} instead. 
  public static final io.grpc.MethodDescriptor<ru.itis.users.grpc.pb.cv.CvRequest,
      ru.itis.users.grpc.pb.cv.CvResponse> METHOD_GET_CV = getGetCvMethod();

  private static volatile io.grpc.MethodDescriptor<ru.itis.users.grpc.pb.cv.CvRequest,
      ru.itis.users.grpc.pb.cv.CvResponse> getGetCvMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<ru.itis.users.grpc.pb.cv.CvRequest,
      ru.itis.users.grpc.pb.cv.CvResponse> getGetCvMethod() {
    io.grpc.MethodDescriptor<ru.itis.users.grpc.pb.cv.CvRequest, ru.itis.users.grpc.pb.cv.CvResponse> getGetCvMethod;
    if ((getGetCvMethod = CvServiceGrpc.getGetCvMethod) == null) {
      synchronized (CvServiceGrpc.class) {
        if ((getGetCvMethod = CvServiceGrpc.getGetCvMethod) == null) {
          CvServiceGrpc.getGetCvMethod = getGetCvMethod = 
              io.grpc.MethodDescriptor.<ru.itis.users.grpc.pb.cv.CvRequest, ru.itis.users.grpc.pb.cv.CvResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "CvService", "GetCv"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ru.itis.users.grpc.pb.cv.CvRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ru.itis.users.grpc.pb.cv.CvResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new CvServiceMethodDescriptorSupplier("GetCv"))
                  .build();
          }
        }
     }
     return getGetCvMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static CvServiceStub newStub(io.grpc.Channel channel) {
    return new CvServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static CvServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new CvServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static CvServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new CvServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class CvServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getCv(ru.itis.users.grpc.pb.cv.CvRequest request,
        io.grpc.stub.StreamObserver<ru.itis.users.grpc.pb.cv.CvResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetCvMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetCvMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                ru.itis.users.grpc.pb.cv.CvRequest,
                ru.itis.users.grpc.pb.cv.CvResponse>(
                  this, METHODID_GET_CV)))
          .build();
    }
  }

  /**
   */
  public static final class CvServiceStub extends io.grpc.stub.AbstractStub<CvServiceStub> {
    private CvServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CvServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CvServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CvServiceStub(channel, callOptions);
    }

    /**
     */
    public void getCv(ru.itis.users.grpc.pb.cv.CvRequest request,
        io.grpc.stub.StreamObserver<ru.itis.users.grpc.pb.cv.CvResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetCvMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class CvServiceBlockingStub extends io.grpc.stub.AbstractStub<CvServiceBlockingStub> {
    private CvServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CvServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CvServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CvServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public ru.itis.users.grpc.pb.cv.CvResponse getCv(ru.itis.users.grpc.pb.cv.CvRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetCvMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class CvServiceFutureStub extends io.grpc.stub.AbstractStub<CvServiceFutureStub> {
    private CvServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CvServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CvServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CvServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ru.itis.users.grpc.pb.cv.CvResponse> getCv(
        ru.itis.users.grpc.pb.cv.CvRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetCvMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_CV = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final CvServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(CvServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_CV:
          serviceImpl.getCv((ru.itis.users.grpc.pb.cv.CvRequest) request,
              (io.grpc.stub.StreamObserver<ru.itis.users.grpc.pb.cv.CvResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class CvServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    CvServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ru.itis.users.grpc.pb.cv.CvOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("CvService");
    }
  }

  private static final class CvServiceFileDescriptorSupplier
      extends CvServiceBaseDescriptorSupplier {
    CvServiceFileDescriptorSupplier() {}
  }

  private static final class CvServiceMethodDescriptorSupplier
      extends CvServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    CvServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (CvServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new CvServiceFileDescriptorSupplier())
              .addMethod(getGetCvMethod())
              .build();
        }
      }
    }
    return result;
  }
}
