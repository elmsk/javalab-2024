package ru.itis.users.service;

import ru.itis.users.dto.CandidateDto;

import java.util.List;

public interface HrService {
    List<CandidateDto> getCandidates();
}
