package ru.itis.users.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.users.dto.CandidateDto;
import ru.itis.users.service.HrService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class HrController {

    private final HrService hrService;

    @GetMapping("/candidates")
    public List<CandidateDto> getCandidates() {
        return hrService.getCandidates();
    }

}
