package ru.itis.users.service.impl;

import net.devh.boot.grpc.client.inject.GrpcClient;
import ru.itis.users.dto.CvDto;
import ru.itis.users.grpc.pb.cv.Cv;
import ru.itis.users.grpc.pb.cv.CvRequest;
import ru.itis.users.grpc.pb.cv.CvResponse;
import ru.itis.users.grpc.pb.cv.CvServiceGrpc;
import ru.itis.users.service.CvService;

import java.util.List;

public class CvServiceGrpcClientImpl implements CvService {
    @GrpcClient("cv-service")
    private CvServiceGrpc.CvServiceBlockingStub client;
    @Override
    public List<CvDto> getCvs() {
        CvResponse resp = client.getCv(CvRequest.getDefaultInstance());

        return resp.getListCvsList().stream().map(this::map).toList();
    }

    protected CvDto map(Cv cv) {
        return CvDto.builder()
                .fullName(cv.getFullName())
                .salary(cv.getSalary())
                .description(cv.getDescription())
                .experience(cv.getExperience())
                .skills(cv.getSkillsList())
                .build();
    }
}
