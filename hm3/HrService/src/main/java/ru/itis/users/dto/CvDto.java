package ru.itis.users.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CvDto {
    public String fullName;
    public int experience;
    public String description;
    public List<String> skills;
    public int salary;
}
