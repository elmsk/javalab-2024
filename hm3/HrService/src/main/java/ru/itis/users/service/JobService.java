package ru.itis.users.service;

import ru.itis.users.dto.JobDto;

import java.util.List;

public interface JobService {
    List<JobDto> getJobs();
}
