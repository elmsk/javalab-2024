package ru.itis.users.service;

import ru.itis.users.dto.CvDto;

import java.util.List;

public interface CvService {
    List<CvDto> getCvs();
}
