package ru.itis.users.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JobDto {
    public String title;
    public String company;
    public int salary;
    public int experience;
    public String description;
    public List<String> skills;
}
