package ru.itis.users.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.users.dto.CandidateDto;
import ru.itis.users.dto.CvDto;
import ru.itis.users.dto.JobDto;
import ru.itis.users.service.CvService;
import ru.itis.users.service.HrService;
import ru.itis.users.service.JobService;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HrServiceImpl implements HrService {

    private final CvService cvService;
    private final JobService jobService;

    @Override
    public List<CandidateDto> getCandidates() {
        List<CvDto> cvs = cvService.getCvs();
        List<JobDto> jobs = jobService.getJobs();
        List<CandidateDto> candidates = new ArrayList<>();

        for (JobDto job: jobs)
            for (CvDto cv: cvs)
                if (isCvSuitable(cv, job))
                    candidates.add(
                            CandidateDto.builder()
                                    .cv(cv)
                                    .job(job)
                                    .build()
                    );

        return candidates;
    }

    private boolean isCvSuitable(CvDto cv, JobDto job){
        if(cv.getExperience() >= job.getExperience() & job.getSalary() >= cv.getSalary()){
            int count = 0;
            List<String> skills = cv.getSkills();
            List<String> jobSkills = job.getSkills();

            for (String skill: jobSkills)
                if(skills.contains(skill)){
                    count++;
                }

            if (jobSkills.size() - count <= 1){
                return true;
            }
        }
        return false;
    }
}
