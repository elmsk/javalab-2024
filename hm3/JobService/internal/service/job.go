package service

import (
	"context"
	"gogo/internal/core"
	"gogo/proto"
)

type JobRepository interface {
	GetJob(ctx context.Context) ([]*core.Job, error)
}

type JobService struct {
	proto.JobServiceServer
	JobRepository JobRepository
}

func NewCVService(cvRepository JobRepository) *JobService {
	return &JobService{
		JobRepository: jobRepository,
	}
}

func (service *JobService) GetCv(ctx context.Context, request *proto.JobRequest) (response *proto.JobResponse, err error) {
	jobs, err := service.JobRepository.GetJob(context.Background())

	if err != nil {
		return nil, err
	}

	res := convert(jobs)

	return &proto.JobResponse{ListJobs: res}, nil

}

func convert(jobs []*core.Job) []*proto.Job {
	res := make([]*proto.Job, 0)

	for i := 0; len(jobs) > i; i++ {
		job := jobs[i]
		res = append(res, &proto.Job{
			Title:    job.Title,
			Company:    job.Company,
			Experience:  uint32(job.Experience),
			Description: job.Description,
			Skills:      job.Skills,
			Salary:      uint32(job.Salary),
		})
	}

	return res
}
