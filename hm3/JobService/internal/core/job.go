package core

import "go.mongodb.org/mongo-driver/bson/primitive"

type CV struct {
	Id          primitive.ObjectID `bson:"_id,omitempty"`
	Title       string             `bson:"title,omitempty"`
	Company     string             `bson:"company,omitempty"`
	Experience  uint               `bson:"experience,omitempty"`
	Salary      uint               `bson:"salary,omitempty"`
	Description string             `bson:"description,omitempty"`
	Skills      []string           `bson:"skills,omitempty"`
}
