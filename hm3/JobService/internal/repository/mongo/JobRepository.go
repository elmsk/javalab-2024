package mongo

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"gogo/internal/core"
	"time"
)

type JobRepository struct {
	collection *mongo.Collection
}

func NewCVRepository(collection *mongo.Collection) *JobRepository {
	return &JobRepository{collection: collection}
}

func (repository *JobRepository) GetJob(ctx context.Context) ([]*core.Job, error) {
	ctxTimeout, cancel := context.WithTimeout(ctx, time.Second*20)
	defer cancel()

	jobChannel := make(chan []*core.Job)
	var err error

	go func() {
		err = repository.getJobs(ctxTimeout, jobChannel)
	}()

	if err != nil {
		return nil, err
	}

	var cvs []*core.Job

	select {
	case <-ctxTimeout.Done():
		break
	case cvs = <-jobChannel:
	}

	return cvs, nil

}

func (repository *JobRepository) getJobs(ctx context.Context, channel chan<- []*core.Job) (err error) {
	result, err := repository.collection.Find(ctx, bson.D{})

	if err != nil {
		return err
	}

	var jobs []*core.Job
	if err = result.All(ctx, &jobs); err != nil {
		panic(err)
	}

	channel <- jobs

	return nil
}
