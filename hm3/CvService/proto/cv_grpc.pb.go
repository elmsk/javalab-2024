// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v5.27.0
// source: proto/cv.proto

package proto

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// CvServiceClient is the client API for CvService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type CvServiceClient interface {
	GetCv(ctx context.Context, in *CvRequest, opts ...grpc.CallOption) (*CvResponse, error)
}

type cvServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewCvServiceClient(cc grpc.ClientConnInterface) CvServiceClient {
	return &cvServiceClient{cc}
}

func (c *cvServiceClient) GetCv(ctx context.Context, in *CvRequest, opts ...grpc.CallOption) (*CvResponse, error) {
	out := new(CvResponse)
	err := c.cc.Invoke(ctx, "/CvService/GetCv", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// CvServiceServer is the server API for CvService service.
// All implementations must embed UnimplementedCvServiceServer
// for forward compatibility
type CvServiceServer interface {
	GetCv(context.Context, *CvRequest) (*CvResponse, error)
	mustEmbedUnimplementedCvServiceServer()
}

// UnimplementedCvServiceServer must be embedded to have forward compatible implementations.
type UnimplementedCvServiceServer struct {
}

func (UnimplementedCvServiceServer) GetCv(context.Context, *CvRequest) (*CvResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetCv not implemented")
}
func (UnimplementedCvServiceServer) mustEmbedUnimplementedCvServiceServer() {}

// UnsafeCvServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to CvServiceServer will
// result in compilation errors.
type UnsafeCvServiceServer interface {
	mustEmbedUnimplementedCvServiceServer()
}

func RegisterCvServiceServer(s grpc.ServiceRegistrar, srv CvServiceServer) {
	s.RegisterService(&CvService_ServiceDesc, srv)
}

func _CvService_GetCv_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CvRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CvServiceServer).GetCv(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/CvService/GetCv",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CvServiceServer).GetCv(ctx, req.(*CvRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// CvService_ServiceDesc is the grpc.ServiceDesc for CvService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var CvService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "CvService",
	HandlerType: (*CvServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetCv",
			Handler:    _CvService_GetCv_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "proto/cv.proto",
}
