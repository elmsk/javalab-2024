package mongo

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"gogo/internal/core"
	"time"
)

type CVRepository struct {
	collection *mongo.Collection
}

func NewCVRepository(collection *mongo.Collection) *CVRepository {
	return &CVRepository{collection: collection}
}

func (repository *CVRepository) GetAll(ctx context.Context) ([]*core.CV, error) {
	ctxTimeout, cancel := context.WithTimeout(ctx, time.Second*20)
	defer cancel()

	cvChannel := make(chan []*core.CV)
	var err error

	go func() {
		err = repository.getCVs(ctxTimeout, cvChannel)
	}()

	if err != nil {
		return nil, err
	}

	var cvs []*core.CV

	select {
	case <-ctxTimeout.Done():
		break
	case cvs = <-cvChannel:
	}

	return cvs, nil

}

func (repository *CVRepository) getCVs(ctx context.Context, channel chan<- []*core.CV) (err error) {
	result, err := repository.collection.Find(ctx, bson.D{})

	if err != nil {
		return err
	}

	var cvs []*core.CV
	if err = result.All(ctx, &cvs); err != nil {
		panic(err)
	}

	channel <- cvs

	return nil
}
