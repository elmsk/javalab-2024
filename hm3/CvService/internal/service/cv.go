package service

import (
	"context"
	"gogo/internal/core"
	"gogo/proto"
)

type CvRepository interface {
	GetAll(ctx context.Context) ([]*core.CV, error)
}

type CVService struct {
	proto.CvServiceServer
	CvRepository CvRepository
}

func NewCVService(cvRepository CvRepository) *CVService {
	return &CVService{
		CvRepository: cvRepository,
	}
}

func (service *CVService) GetCv(ctx context.Context, request *proto.CvRequest) (response *proto.CvResponse, err error) {
	cvs, err := service.CvRepository.GetAll(context.Background())

	if err != nil {
		return nil, err
	}

	res := convert(cvs)

	return &proto.CvResponse{ListCvs: res}, nil

}

func convert(cvs []*core.CV) []*proto.Cv {
	res := make([]*proto.Cv, 0)

	for i := 0; len(cvs) > i; i++ {
		cv := cvs[i]
		res = append(res, &proto.Cv{
			FullName:    cv.FullName,
			Experience:  uint32(cv.Experience),
			Description: cv.Description,
			Skills:      cv.Skills,
			Salary:      uint32(cv.Salary),
		})
	}

	return res
}
