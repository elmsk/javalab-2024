package core

import "go.mongodb.org/mongo-driver/bson/primitive"

type CV struct {
	Id          primitive.ObjectID `bson:"_id,omitempty"`
	FullName    string             `bson:"fullName,omitempty"`
	Experience  uint               `bson:"experience,omitempty"`
	Description string             `bson:"description,omitempty"`
	Skills      []string           `bson:"skills,omitempty"`
	Salary      uint               `bson:"salary,omitempty"`
}
