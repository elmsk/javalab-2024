package ru.itis.hotelservice.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.hotelservice.controllers.api.HotelApi;
import ru.itis.hotelservice.dto.HotelDto;
import ru.itis.hotelservice.services.HotelService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class HotelController implements HotelApi {

    HotelService hotelService;

    @Override
    public ResponseEntity<List<HotelDto>> getCinemas(String cityName) {
        List<HotelDto> hotels = hotelService.getCityHotels(cityName);

        if (hotels == null || hotels.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        return ResponseEntity.ok(hotels);
    }
}

