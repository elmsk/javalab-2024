package ru.itis.hotelservice.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.hotelservice.models.Hotel;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(name = "cinema", description = "кинотеатр")
public class HotelDto {

    @Schema(description = "идентификатор", example = "1")
    private Long id;

    @Schema(description = "наименование кинотеатра", example = "Kinomax Metropolis", maxLength = 255)
    private String name;

    @Schema(description = "адрес", example = "3-й джавовский проезд, д. 17", maxLength = 40)
    private String address;

    @Schema(description = "город", example = "Казань", maxLength = 40)
    private String cityName;

    public static HotelDto from(Hotel entity) {
        return HotelDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .address(entity.getAddress())
                .cityName(entity.getCityName())
                .build();
    }

    public static List<HotelDto> from(List<Hotel> list) {
        return list.stream()
                .map(HotelDto::from)
                .collect(Collectors.toList());
    }
}


