package ru.itis.hotelservice.services;

import ru.itis.hotelservice.dto.HotelDto;

import java.util.List;

public interface HotelService {
    List<HotelDto> getCityHotels(String cityName);
}
